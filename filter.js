import { ControllerPackage } from "./agency_travel/CONTROLLER/package.js";
import { ControllerCountry } from "./agency_travel/CONTROLLER/country.js";
import { ControllerEmployee } from "./agency_travel/CONTROLLER/employee.js";

const btn = document.getElementById("btn-filter-area")


let selectCountryFilter;
let selectGuideFilter;
let captureDateFilter;
const captureReferencedInputs = function(){
    selectCountryFilter = document.getElementById("select-country-filter")
    selectGuideFilter = document.getElementById("select-guide-filter")
    captureDateFilter = document.getElementById("capture-date-filter")
}

let contriesControllerGet = new ControllerCountry()
let packagesControllerGet = new ControllerPackage()
let employeeControllerGet = new ControllerEmployee()

let countriesDataList;
let packagesDataList;
let employeeDataList;

const getAllNecesaryData = async function(){
    countriesDataList = await contriesControllerGet.getCountries();
    packagesDataList = await packagesControllerGet.getPackages();
    employeeDataList = await employeeControllerGet.getEmployees();
}

const createOptionCountries = function(){
    let htmlString = "<option value='all'>All</option>"
    for (let a = 0; a< countriesDataList.length ; a++){
        htmlString = htmlString + `<option value="${countriesDataList[a].name}">${countriesDataList[a].name}</option>`
    }
    return htmlString
}

const createOptionGuiders = function(){
    let htmlString = "<option value='all'>All</option>"
    let listGuiders = [...new Set(packagesDataList.map((ele)=>{
        return ele.employee_id
    }))]
    console.log(listGuiders)
    // console.log(listNamesGuiders)
    // const listGuiders = listNamesGuiders.filter((item,index) => listNamesGuiders.indexOf(item)  == index);
    //const listGuiders = packagesDataList.filter(employ => employ.type_employee == 3);
    for (let a = 0; a< listGuiders.length ; a++){
        htmlString = htmlString + `<option value="${listGuiders[a]}">${listGuiders[a]}</option>`
    }
    return htmlString
}


const rechargeAreaPackages = function(lisCountries, lisPackages){
    let html_string = "";
    let position;
    for(let i =0 ; i < lisPackages.length ; i++){
        if(verifySesion()){
            btnIniciarModal = `
            <div>
            <button class="package-get btn-package-detail" id ="${lisPackages[i].id}">Go</button>
            </div>
            `
        }
        position = lisPackages[i].country_id

        html_string = html_string + `
        <div class="package">
            <div class = "package-wall">
                <img class = "package-img" src="../${lisPackages[i].image}" alt="">
                <div class="package-price">Desde<br>$${lisPackages[i].price}</div>
                <h3 class="package-days">${lisPackages[i].days}</h3>
            </div>
            <h1 class="package-name">${lisPackages[i].name}</h2>
            <h3 class= "package-country">${(lisCountries.find(ele => ele.id == position)).name}</h3>
            <div class= "package-booked">
                <h3 class= "package-available">Available:${lisPackages[i].able}</h3>
                <h3 class= "package-reserved">Reserved:${lisPackages[i].reserved}</h3>
            </div>
            
            <h3 class="package-guide">Guide: ${lisPackages[i].employee_id}</h3>
            ${btnIniciarModal}
        </div>
    `
    }

    return html_string
}


const buildHtmlAreaFilter = function(){
    captureReferencedInputs();
    selectCountryFilter.innerHTML = createOptionCountries()
    selectGuideFilter.innerHTML = createOptionGuiders()
}

let opcionCountryFilter;
let opcionGuideFilter;
let opcionDateFilter;

const captureInputsValues = function(){
    // optionsSelect.options[optionsSelect.selectedIndex].value;
    opcionCountryFilter = selectCountryFilter.options[selectCountryFilter.selectedIndex].value
    opcionGuideFilter = selectGuideFilter.options[selectGuideFilter.selectedIndex].value
    opcionDateFilter = captureDateFilter.value
}

await getAllNecesaryData();
buildHtmlAreaFilter()


btn.addEventListener("click",() => {
    captureInputsValues()
    if(opcionCountryFilter != "all"){
        packagesDataList = packagesDataList.filter(pk => pk.country_id == countriesDataList.find(ele => ele.name == opcionCountryFilter).id);
        console.log(packagesDataList)
    }
    
    if(opcionGuideFilter != "all"){
        packagesDataList = packagesDataList.filter(pk => pk.employee_id == opcionGuideFilter);
        console.log(packagesDataList)
    }

    if(opcionDateFilter.length != 0){
        console.log(opcionDateFilter)
        packagesDataList = packagesDataList.filter(pk => pk.date == opcionDateFilter);
        console.log(packagesDataList)
    }

    rechargeAreaPackages(countriesDataList, packagesDataList)

    getAllNecesaryData();
})

