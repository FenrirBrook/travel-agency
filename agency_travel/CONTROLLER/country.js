import { Conection } from "../access/conection.js";

export class ControllerCountry{

    constructor(){
        this.conection = new Conection("countries")
    }

    getCountries(){
        return this.conection.getData()
    }

    getCountryById(idObj){
        return this.conection.getElementId(idObj)
    }

    addCountry(objCountry){
        this.conection.addElement(objCountry)
    }

    updateCountry(objCountry){
        this.conection.updateElement(objCountry)
    }

    deleteCountry(objCountry){
        this.conection.deleteElement(objCountry)
    }

    

    validateObjectCountry(objCountry){
        if(isNaN(objCountry.id) == false && objCountry.name !== ""){
            return true
        }
        return false
    }
}