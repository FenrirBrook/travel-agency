import { Conection } from "../access/conection.js";

export class ControllerEmployee{

    constructor(){
        this.conection = new Conection("employees")
    }

    getEmployees(){
        return this.conection.getData()
    }

    getEmployeeById(idObj){
        return this.conection.getElementId(idObj)
    }

    addEmployee(objEmployee){
        this.conection.addElement(objEmployee)
    }

    updateEmployee(objEmployee){
        this.conection.updateElement(objEmployee)
    }

    deleteEmployee(objEmployee){
        this.conection.deleteElement(objEmployee)
    }

    

    validateObjectEmployee(objEmployee){
        if(isNaN(objEmployee.id) == false && objEmployee.name !== ""){
            return true
        }
        return false
    }
}