import { Conection } from "../access/conection.js";

export class ControllerPackage{

    constructor(){
        this.conection = new Conection("packages")
    }

    getPackages(){
        return this.conection.getData()
    }

    getPackageById(idObj){
        return this.conection.getElementId(idObj)
    }

    getPackageByCountry(idcountrie){
        return this.conection
    }

    addPackage(objPackage){
        this.conection.addElement(objPackage)
    }

    updatePackage(objPackage){
        this.conection.updateElement(objPackage)
    }

    deletePackage(objPackage){
        this.conection.deleteElement(objPackage)
    }

    

    validateObjectPackage(objPackage){
        if(isNaN(objPackage.id) == false && objPackage.name !== ""){
            return true
        }
        return false
    }
}