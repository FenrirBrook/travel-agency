import { Conection } from "../access/conection.js";

export class ControllerClient{

    constructor(){
        this.conection = new Conection("clients")
    }

    getClients(){
        return this.conection.getData()
    }

    getClientById(idObj){
        return this.conection.getElementId(idObj)
    }

    addClient(objClient){
        this.conection.addElement(objClient)
    }

    updateClient(objClient){
        this.conection.updateElement(objClient)
    }

    deleteClient(objClient){
        this.conection.deleteElement(objClient)
    }

    

    validateObjectClient(objClient){
        if(isNaN(objClient.id) == false && objClient.name !== ""){
            return true
        }
        return false
    }
}