import { Conection } from "../access/conection.js";

export class ControllerDestination{

    constructor(){
        this.conection = new Conection("destinations")
    }

    getDestinations(){
        return this.conection.getData()
    }

    getDestinationById(idObj){
        return this.conection.getElementId(idObj)
    }

    addDestination(objDestination){
        this.conection.addElement(objDestination)
    }

    updateDestination(objDestination){
        this.conection.updateElement(objDestination)
    }

    deleteDestination(objDestination){
        this.conection.deleteElement(objDestination)
    }

    

    validateObjectEmployee(objEmpobjDestinationloyee){
        if(isNaN(objDestination.id) == false && objDestination.name !== ""){
            return true
        }
        return false
    }
}