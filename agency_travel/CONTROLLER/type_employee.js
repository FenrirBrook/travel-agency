import { Conection } from "../access/conection.js";

export class ControllerTypeEmployee{

    constructor(){
        this.conection = new Conection("type_employees")
    }

    getTypeEmployees(){
        return this.conection.getData()
    }

    getTypeEmployeeById(idObj){
        return this.conection.getElementId(idObj)
    }

    addTypeEmployee(objTypeEmployee){
        this.conection.addElement(objTypeEmployee)
    }

    updateTypeEmployee(objTypeEmployee){
        this.conection.updateElement(objTypeEmployee)
    }

    deleteTypeEmployee(objTypeEmployee){
        this.conection.deleteElement(objTypeEmployee)
    }

    validateObjectPackage(objTypeEmployee){
        if(isNaN(objTypeEmployee.id) == false && objTypeEmployee.name !== ""){
            return true
        }
        return false
    }
}