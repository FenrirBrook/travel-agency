import { Conection } from "../access/conection.js";

export class ControllerReservation{

    constructor(){
        this.conection = new Conection("reservations")
    }

    getReservations(){
        return this.conection.getData()
    }

    getReservationById(idObj){
        return this.conection.getElementId(idObj)
    }

    addReservation(objReservation){
        this.conection.addElement(objReservation)
    }

    updateReservation(objReservation){
        this.conection.updateElement(objReservation)
    }

    deleteReservation(objReservation){
        this.conection.deleteElement(objReservation)
    }

    

    validateObjectPackage(objReservation){
        if(isNaN(objReservation.id) == false && objReservation.name !== ""){
            return true
        }
        return false
    }
}