export class Package{
    constructor(name, country_id, employee_id, price, days, able, reserved, image, id){
        
        this.name = name;
        this.country_id = country_id;
        this.employee_id= employee_id;
        this.price= price;
        this.days= days;
        this.able= able;
        this.reserved= reserved;
        this.image= image;
        this.id = id;
    }
}