class Employee{
    constructor(id, name, last_name, email, password, dni, type_employee){
        this.id = id;
        this.name = name;
        this.last_name= last_name;
        this.email = email;
        this.password = password;
        this.dni = dni;
        this.type_employee = type_employee;
    }
}