export class Destination{
    constructor(id, name, url, obj_country, description, list_objTags){
        this.id = id;
        this.name = name
        this.url = url
        this.objCountry = obj_country
        this.description = description
        this.listObjTags = list_objTags
    }
}