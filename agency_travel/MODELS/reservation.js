export class Reservation{

    constructor(id, name, packageId, cliente_id, country, count, price, detalles,employee_seller_id="Luis Miguel",image, days="4 days / 3 night", date="12/18/2022"){
        this.id = id;
        this.name = name;
        this.package_id = packageId;
        this.cliente_id = cliente_id;
        this.country = country;
        this.count = count;
        this.price = price;
        this.detalles = detalles
        this.employee_seller_id= employee_seller_id;
        this.image = image;
        this.days = days;
        this.date = date;
    }
}