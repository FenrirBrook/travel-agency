const lista = document.querySelectorAll(".btn-see-details-destination")
const listaboxes = document.querySelectorAll(".item-identifier")



const cambiarTexto = function(id_key,name_id){ 
    
    return `
                <div class="item-destination-page-selected" >
                    <div class="image-item-destination" style="background-image: url(https://static.toiimg.com/thumb/msid-87867224,width-748,height-499,resizemode=4,imgsize-232412/.jpg);">
                        <div class="image-item-destination-size">
    
                        </div>
                    </div>
                    <div class="info-item-destination-selected">
                        <h2 class="title-destination-selected">Titulo Name ${name_id}</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti expedita quibusdam incidunt tempora minima animi accusamus, beatae ullam distinctio dolores ut dignissimos, adipisci temporibus, aspernatur optio deserunt ipsum voluptatem nesciunt.</p>
                        <div>
                            <p>country</p>
                            <p>nombreCountry</p>
                        </div>
                        <div>
                            TAGS
                        </div>
    
                        <div class="buttons-destination">
                            <button id= "btn${id_key}">Close</button>
                            <button>Make Reservation</button>
                        </div>
                    </div>
                </div>
`
}

// const cambiarTextoOriginal = function(identifier){

// }

const realText = `
                <div  class="item-destination-page" style="background-image: url(https://static.toiimg.com/thumb/msid-87867224,width-748,height-499,resizemode=4,imgsize-232412/.jpg);">
                    <div class="info-item-destination">
                        <h2 class="title-destination">Titulo Name</h2>
                        <div class="buttons-destination">
                            <button class = "btn-see-details-destination">Details</button>
                            <button>Make Reservation</button>
                        </div>
                    </div>
                </div>
`


for(let e = 0 ; e < lista.length ; e++){
    lista[e].addEventListener("click",()=>{
        let findBoxShower = document.getElementById("show-item-selected")
        let uniqueIdentifier = listaboxes[e].id
        findBoxShower.innerHTML = cambiarTexto(uniqueIdentifier, uniqueIdentifier)

        let btnIdentifier = document.getElementById("btn"+uniqueIdentifier)
        btnIdentifier.addEventListener("click",()=>{
            findBoxShower.innerHTML = ""
        })
        location.assign("#show-item-selected")
    })
}