import { ControllerPackage } from "../CONTROLLER/package.js"
import { ControllerCountry } from "../CONTROLLER/country.js";
import { ControllerEmployee } from "../CONTROLLER/employee.js";
import { buildDetailPackageForm } from "./components/detail_package_form.js";
import { ControllerReservation } from "../CONTROLLER/reservation.js";
import { ControllerClient } from "../CONTROLLER/client.js";
import { verifySesion } from "../authentication/auth.js";
import { Reservation } from "../MODELS/reservation.js";
import { Client } from "../MODELS/client.js";
import { Package} from "../MODELS/package.js";
import { createPackageForm } from "./components/create_package_form.js";


let btnIniciarModal = "<a href='#footer-container'><h3>Contact Us</h3></a>"


const new_reservation = new ControllerReservation();
const new_client = new ControllerClient();


const new_package = new ControllerPackage();
const new_country = new ControllerCountry();


let newIdReservation;

let listButtonsPackages;
let btnCerrarModal;
let btnMakeReservation;
 
let containerFormPackageDetail = document.getElementById("container-form-package-detail")
const idModalPackageDetail = document.getElementById("modal-packages-form");
const packagesBox = document.getElementById('packages-box');


const areaBtnCreateNewPackage = document.getElementById("area-btn-add-packages")
const textBtnCreatePackage = "<button id='btn-show-modal-create-package'>Add New Package</button>"
const modalCreatePackageContainer = document.getElementById("modal-create-packages-form")
let btnCloseCreatePackageModal;
let areaToAddForm;
let btnShowModalCreatePackage

const ctr_package = new ControllerPackage();
const elm = await ctr_package.getPackages();

const createPackageFunction = function(){
    let btninsert = document.getElementById("btn-save");
    console.log(btninsert)
    let name = ''
    let country = ''
    let guide = ''
    let price =''
    let days = ''
    let img = ''
    let able = 17
    let reserved = 0
    
    let id= parseInt(elm[elm.length-1].id + 1)

    
btninsert.addEventListener("click",(e)=>{
    e.preventDefault();
    
    name = document.getElementById('form-name').value;
    country = document.getElementById('form-country').value;
    guide = document.getElementById('form-guide').value;
    price = parseFloat(document.getElementById('form-price').value);
    days = document.getElementById('form-days').value;
    img = '/IMAGES/' + document.getElementById('form-img').files[0].name;
    let imgurl = document.getElementById('form-img').files[0];
    console.log(name)
    const md_package = new Package(name,country,guide, price, days,able,reserved, img,id)
    ctr_package.addPackage(md_package);
})
}  




const closeBtnCreatePackageModal = function(){
    modalCreatePackageContainer.className = "ocultar-modal"
}

const showBtnCreatePackageModal = function(){
    modalCreatePackageContainer.className = ""
    areaToAddForm = document.getElementById("container-modal-create-package-form")
    areaToAddForm.innerHTML = createPackageForm
    btnCloseCreatePackageModal = document.getElementById('btn-cerrar-modal-create');
    btnCloseCreatePackageModal.addEventListener('click',closeBtnCreatePackageModal)
    createPackageFunction()
}





if (verifySesion()) {
    areaBtnCreateNewPackage.innerHTML = textBtnCreatePackage
    btnShowModalCreatePackage = document.getElementById("btn-show-modal-create-package")
    btnShowModalCreatePackage.addEventListener('click', showBtnCreatePackageModal)
}


let data_list; 
let data_list_country; 
let data_reservations;

const buildPackages = async function (){
    let html_string = "";
    let position;

    data_reservations = await new_reservation.getReservations();
    newIdReservation = data_reservations.length + 1

    data_list = await new_package.getPackages()
    data_list_country = await new_country.getCountries()
    

    for(let i =0 ; i < data_list.length ; i++){

        if(verifySesion()){
            btnIniciarModal = `<button class="package-get btn-package-detail" id ="${data_list[i].id}">Reserve</button>`
        }

        position = data_list[i].country_id

        html_string = html_string + `
        <div class="package">
            <div class = "package-wall">
                <img class = "package-img" src="../${data_list[i].image}" alt="">
                <div class="package-price">Desde<br>$${data_list[i].price}</div>
                <h3 class="package-days">${data_list[i].days}</h3>
            </div>
            <h1 class="package-name">${data_list[i].name}</h2>
            <h3 class= "package-country">${(data_list_country.find(ele => ele.id == position)).name}</h3>
            <div class= "package-booked">
                <h3 class= "package-available">Available:${data_list[i].able}</h3>
                <h3 class= "package-reserved">Reserved:${data_list[i].reserved}</h3>
            </div>
            
            <h3 class="package-guide">Guide: ${data_list[i].employee_id}</h3>
            ${btnIniciarModal}
        </div>
    `
    }

    return html_string
}


const addBotonModalInsertPackage = function (){
    listButtonsPackages = document.querySelectorAll(".btn-package-detail");

    for(let i =0; i< listButtonsPackages.length; i++){
        let new_obj = data_list.find(key => key.id == listButtonsPackages[i].id)

        let newNameCountry = data_list_country.find(key => key.id == data_list[i].country_id).name
        
        listButtonsPackages[i].addEventListener('click',()=>{
            idModalPackageDetail.className = ""
            containerFormPackageDetail.innerHTML = buildDetailPackageForm(new_obj.id,new_obj.name, newNameCountry, new_obj.price, new_obj.days,new_obj.able, new_obj.reserved);
        })

    }

    btnMakeReservation = document.querySelector(".btn-insert-package")
    //btnMakeReservation.id = 
    btnMakeReservation.addEventListener('click', ()=>{
        let ObjReservation;
        let newObjClient;

        let priceReservation = 0.0;
        let employeeIdSeller = 1
        let reservationDetails = ""
        let countPeople = parseFloat(document.querySelector("#people").value)
        let newNameReservation = document.querySelector(".package-name").textContent
        let newPackageId  =data_list.find(ele => ele.name == newNameReservation ).id

        let imageReservation = data_list.find(key => key.id == newPackageId).image;
        console.log(imageReservation)
        let countryName = document.querySelector(".package-country").textContent

        newObjClient = new Client(document.getElementById("idcliente").value, document.getElementById("nombrecliente").value);

        priceReservation = parseFloat(document.querySelector(".package-price").textContent) +  priceReservation

            
            if (document.getElementById('hotel').checked)
                {
                priceReservation = priceReservation + 50.0
                reservationDetails = reservationDetails + " Hotel" 
                }
                if (document.getElementById('feed').checked)
                {
                priceReservation = priceReservation + 100.0
                reservationDetails = reservationDetails + " Feed" 
                }
                if (document.getElementById('transport').checked)
                {
                priceReservation = priceReservation + 200.0
                reservationDetails = reservationDetails + " Transport" 
                }
                priceReservation = priceReservation * countPeople

                console.log("masdasd : ", priceReservation)

        ObjReservation = new Reservation(newIdReservation,newNameReservation,newPackageId,newObjClient,countryName, countPeople, priceReservation, reservationDetails,employeeIdSeller,imageReservation);

        new_reservation.addReservation(ObjReservation);

    })



    btnCerrarModal =document.getElementById("btn-cerrar-modal")
    console.log(btnCerrarModal)
    btnCerrarModal.addEventListener('click', ()=>{
        idModalPackageDetail.className = "ocultar-modal"
    })
    
    

}

const packageContent = await buildPackages() 

packagesBox.innerHTML = packageContent;

addBotonModalInsertPackage();




const btn = document.getElementById("btn-filter-area")


let selectCountryFilter;
let selectGuideFilter;
let captureDateFilter;
const captureReferencedInputs = function(){
    selectCountryFilter = document.getElementById("select-country-filter")
    selectGuideFilter = document.getElementById("select-guide-filter")
    captureDateFilter = document.getElementById("capture-date-filter")
}

let contriesControllerGet = new ControllerCountry()
let packagesControllerGet = new ControllerPackage()
let employeeControllerGet = new ControllerEmployee()

let countriesDataList;
let packagesDataList;
let employeeDataList;

const getAllNecesaryData = async function(){
    countriesDataList = await contriesControllerGet.getCountries();
    packagesDataList = await packagesControllerGet.getPackages();
    employeeDataList = await employeeControllerGet.getEmployees();
}

const createOptionCountries = function(){
    let htmlString = "<option value='all'>All</option>"
    for (let a = 0; a< countriesDataList.length ; a++){
        htmlString = htmlString + `<option value="${countriesDataList[a].name}">${countriesDataList[a].name}</option>`
    }
    return htmlString
}

const createOptionGuiders = function(){
    let htmlString = "<option value='all'>All</option>"
    let listGuiders = [...new Set(packagesDataList.map((ele)=>{
        return ele.employee_id
    }))]
    console.log(listGuiders)
    // console.log(listNamesGuiders)
    // const listGuiders = listNamesGuiders.filter((item,index) => listNamesGuiders.indexOf(item)  == index);
    //const listGuiders = packagesDataList.filter(employ => employ.type_employee == 3);
    for (let a = 0; a< listGuiders.length ; a++){
        htmlString = htmlString + `<option value="${listGuiders[a]}">${listGuiders[a]}</option>`
    }
    return htmlString
}


const rechargeAreaPackages = function(lisCountries, lisPackages){
    let html_string = "";
    let position;
    for(let i =0 ; i < lisPackages.length ; i++){
        if(verifySesion()){
            btnIniciarModal = `
            <div>
            <button class="package-get btn-package-detail" id ="${lisPackages[i].id}">Reserve</button>
            </div>
            `
        }
        position = lisPackages[i].country_id

        html_string = html_string + `
        <div class="package">
            <div class = "package-wall">
                <img class = "package-img" src="../${lisPackages[i].image}" alt="">
                <div class="package-price">Desde<br>$${lisPackages[i].price}</div>
                <h3 class="package-days">${lisPackages[i].days}</h3>
            </div>
            <h1 class="package-name">${lisPackages[i].name}</h2>
            <h3 class= "package-country">${(lisCountries.find(ele => ele.id == position)).name}</h3>
            <div class= "package-booked">
                <h3 class= "package-available">Available:${lisPackages[i].able}</h3>
                <h3 class= "package-reserved">Reserved:${lisPackages[i].reserved}</h3>
            </div>
            
            <h3 class="package-guide">Guide: ${lisPackages[i].employee_id}</h3>
            ${btnIniciarModal}
        </div>
    `
    }

    return html_string
}


const buildHtmlAreaFilter = function(){
    captureReferencedInputs();
    selectCountryFilter.innerHTML = createOptionCountries()
    selectGuideFilter.innerHTML = createOptionGuiders()
}

let opcionCountryFilter;
let opcionGuideFilter;
let opcionDateFilter;

const captureInputsValues = function(){
    // optionsSelect.options[optionsSelect.selectedIndex].value;
    opcionCountryFilter = selectCountryFilter.options[selectCountryFilter.selectedIndex].value
    opcionGuideFilter = selectGuideFilter.options[selectGuideFilter.selectedIndex].value
    opcionDateFilter = captureDateFilter.value
}

await getAllNecesaryData();
buildHtmlAreaFilter()


btn.addEventListener("click",() => {
    captureInputsValues()
    if(opcionCountryFilter != "all"){
        packagesDataList = packagesDataList.filter(pk => pk.country_id == countriesDataList.find(ele => ele.name == opcionCountryFilter).id);
        console.log(packagesDataList)
    }
    
    if(opcionGuideFilter != "all"){
        packagesDataList = packagesDataList.filter(pk => pk.employee_id == opcionGuideFilter);
        console.log(packagesDataList)
    }

    if(opcionDateFilter.length != 0){
        console.log(opcionDateFilter)
        packagesDataList = packagesDataList.filter(pk => pk.date == opcionDateFilter);
        console.log(packagesDataList)
    }

    packagesBox.innerHTML = rechargeAreaPackages(countriesDataList, packagesDataList)
    addBotonModalInsertPackage();
    getAllNecesaryData();
})

