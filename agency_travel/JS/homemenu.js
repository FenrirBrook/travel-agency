import { overWriteHomeMenu, cambiarMenuEmployees, overWriteHomeMenuLogged } from "./components/navigation.js";
import { getRolSesion } from "../authentication/auth.js";


if(getRolSesion() == "ADMIN" || getRolSesion() == "USER"){
    cambiarMenuEmployees();
    overWriteHomeMenuLogged();
}else{
    overWriteHomeMenu();
}

