let ITEMS_MENU_ADMIN = ["home","packages","reservation","reports","Login"]
let URLS_MENU_ADMIN = ["../home","./packages","./reservations","./reports","./login"]

let ITEMS_MENU_EMPLOYEE = ["home","packages","reservation","login"]
let URLS_MENU_EMPLOYEE = ["../home","./packages","./reservations","./login"]

let ITEMS_MENU_GUEST = ["home","packages","login"]
let URLS_MENU_GUEST = ["../home","./packages","./login"]


let logoPath = "../IMAGES/logo.jpg"
let btnSalirContent = ""

export const overWriteHomeMenu = function(){
    URLS_MENU_ADMIN = ["./home","./PAGES/packages","./PAGES/reservations","./PAGES/reports","./PAGES/login"]
    URLS_MENU_EMPLOYEE = ["home","./PAGES/packages","./PAGES/reservations","./PAGES/login"]
    URLS_MENU_GUEST = ["./home","./PAGES/packages","./PAGES/login"]
    logoPath = "./IMAGES/logo.jpg"
}

export const cambiarMenuEmployees = function(){
    ITEMS_MENU_ADMIN = ["home","packages","reservation","reports"]
    URLS_MENU_ADMIN = ["../home","./packages","./reservations","./reports"]

    ITEMS_MENU_EMPLOYEE = ["home","packages","reservation"]
    URLS_MENU_EMPLOYEE = ["../home","./packages","./reservations"]

    ITEMS_MENU_GUEST = ["home","packages"]
    URLS_MENU_GUEST = ["../home","./packages"]
    logoPath = "../IMAGES/logo.jpg"
    btnSalirContent = "<button id='btn-salir'>salir</button>"
}


export const overWriteHomeMenuLogged = function(){
    URLS_MENU_ADMIN = ["./home","./PAGES/packages","./PAGES/reservations","./PAGES/reports"]
    URLS_MENU_EMPLOYEE = ["home","./PAGES/packages","./PAGES/reservations"]
    URLS_MENU_GUEST = ["./home","./PAGES/packages"]
    logoPath = "./IMAGES/logo.jpg"
    btnSalirContent = "<button id='btn-salir'>salir</button>"
}






const generateMenuItems = function(rol_id){
    let textItemsmenu = ""
    if(rol_id == "ADMIN"){
        textItemsmenu = buildMenu(ITEMS_MENU_ADMIN,URLS_MENU_ADMIN);
    }
    if(rol_id == "USER"){
        textItemsmenu = buildMenu(ITEMS_MENU_EMPLOYEE,URLS_MENU_EMPLOYEE);
    }
    if(rol_id == "GUEST"){
        textItemsmenu = buildMenu(ITEMS_MENU_GUEST,URLS_MENU_GUEST);
    }
    
    return textItemsmenu
}

const buildMenu = function (list_options,list_urls){
    let html_string = "";
    for(let i =0 ; i < list_options.length ; i++){

        html_string = html_string + `<li class = "nav-item"><a href="${list_urls[i]}.html">${list_options[i]}</a></li>`
    }
    return html_string
}


const ObtainMenu = function(id_rol){
    let result = generateMenuItems(id_rol);

    const navigation= `

    <nav class = "nav-container">
        <input type="checkbox" id="check">
        <label for="check" class="nav-checkbtn">
            <i class="fas fa-bars"></i>
        </label>
        <a href="#" class="nav-logo">
            <img src="${logoPath}" alt="" class="nav-img">
            <h1 class = "nav-title-logo">EuroFlys</h1>
        </a>
        <ul class = "nav-items">
            ${result}
            ${btnSalirContent}
        </ul>
        
    </nav>
`
    return navigation
}


export {
    ObtainMenu
}


