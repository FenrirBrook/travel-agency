export const createPackageForm = `
<form id="add-package-form" class="add-package-form">
    <h1>Create Package</h1>
    <input id="form-name" class="inp" type="text" placeholder="Package Name">
    <input id="form-price" class="inp" type="text" placeholder="Price">
    <input id="form-days" class="inp" type="text" placeholder="Days">
    <input id="form-guide" class="inp" type="text" placeholder="Guide">
    <input id="form-country" class="inp" type="text" placeholder="Country">
    <label for="">Package Image: <input id="form-img" type="file"></label>
    <button id="btn-save" class="btn-save">Save</button>
</form>
`
