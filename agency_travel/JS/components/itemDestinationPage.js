



export const CreateItemDestinationInPage = function({id,name, url, description, obj_country, listTags}){
    let htmlText =  `
        <div id="${id}" class="item-identifier">
            <div  class="item-destination-page" style="background-image: url(${url});">
                <div class="info-item-destination">
                    <h2 class="title-destination">${name}</h2>
                    <div class="buttons-destination">
                        <button class = "btn-see-details-destination">Details</button>
                        <button>Make Reservation</button>
                    </div>
                </div>
            </div>
        </div>
`
return htmlText
}


const generateTags = function(listObjTags){
    let textTags = ""
    for(let i =0; i<listObjTags.length ; i ++){
        textTags = textTags + "<p>"+ listObjTags[i].name +"</p>"
    }
    return textTags
}


export const CreateDetailedItemDestination = function({id,name, url, description, obj_country, listObjTags}){

    
let htmlText = `
    <div class="item-destination-page-selected" >
        <div class="image-item-destination" style="background-image: url(${url});">
            <div class="image-item-destination-size">

            </div>
        </div>
        <div class="info-item-destination-selected">
            <h2 class="title-destination-selected">${name}</h2>
            <p>${description}</p>
            <div>
                <p>${obj_country.name}</p>
                <p>nombreCountry</p>
            </div>
            <div>
                ${generateTags(listObjTags)}
            </div>

            <div class="buttons-destination">
                <button id= "btn${id}">Close</button>
                <button>Make Reservation</button>
            </div>
        </div>
    </div>
`
return htmlText
}