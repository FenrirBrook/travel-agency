const rowDataReport = `
<div class="data-report">
<div class="container-headers">
        <p>data</p>
        <p>data</p>
        <p>data</p>
        <p>data</p>
        <p>data</p>
</div>
<div class="container-data">
    <div class="row-data">
        <p>data</p>
        <p>data</p>
        <p>data</p>
        <p>data</p>
        <p>data</p>
    </div>
    <div class="row-data">
        <p>data</p>
        <p>data</p>
        <p>data</p>
        <p>data</p>
        <p>data</p>
    </div>
</div>
</div>
`
const headerClients= ["id","name","dni","gmail"]
const headerPackages= ["id","name","country_id","employee_id","price","days","able","date"]
const headerReservations= ["id","name","package_id","days","country","price","date","detalles"  ]


const generateRowHtml = function(list_data, listKeys){
    let string_html = "<div class='container-data'>";
    let row_html;
    for(let i =0; i< list_data.length ; i++){
        if (i % 2 == 1){
            row_html = "<div class='row-data row-data-even'>";
        }else{
            row_html = "<div class='row-data'>";
        }   
        for(let j =0; j< listKeys.length ; j++){
            row_html = row_html + "<p>" + list_data[i][listKeys[j]]+ "</p>";
        }
        row_html = row_html + "</div>";
        string_html = string_html + row_html;
    }
    string_html = string_html  + "</div>";
    return string_html
}


const generateHeaderReport = function(listData){
    let string_html = "<div class='container-headers-report'>";
    for(let i = 0; i < listData.length ; i++){
        string_html = string_html + "<p>" + listData[i] + "</p>";
    }
    string_html = string_html + "</div>"
    return string_html
}

const generateAreaReports = function(listData , option_str_selected){
    let string_html = "<div class='data-report'>";
    let headerText = ""
    let contenText = ""
    if(option_str_selected == "clients"){
        string_html = headerText + generateHeaderReport(headerClients)
        string_html = string_html + contenText + generateRowHtml(listData,headerClients)
    }
    if(option_str_selected == "packages"){
        string_html = headerText + generateHeaderReport(headerPackages)
        string_html = string_html + contenText + generateRowHtml(listData,headerPackages)
    }

    if(option_str_selected == "reservations"){
        string_html = headerText + generateHeaderReport(headerReservations)
        string_html = string_html + contenText + generateRowHtml(listData,headerReservations)
    }

    string_html = string_html + "</div>"

    return string_html
}


export {
    generateAreaReports
}