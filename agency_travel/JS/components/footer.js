const footer = `
<div class="footer-container">
        <img class="footer-logo" src="../IMAGES/logo.jpg" alt="">
        <div class = "footer-menu-container">
            <ul>
                <li class="menu-item">Contact 30000</li>
                <li class="menu-item">Email euroflyes@gmail.com</li>
            </ul>
            
        </div>
    </div>
    <div class="copyrigth-container">
        <span class="copyrigth">&copy;2022, EuroFlyes. All rigths reserved</span>
    </div>
`

document.getElementById("footer-container").innerHTML = footer
