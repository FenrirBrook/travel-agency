import { ControllerReservation } from "../CONTROLLER/reservation.js";

const new_reservation = new ControllerReservation();
const packagesBox = document.getElementById('packages-box');
let data_list; 
let data_list_country; 
let data_reservations;


const buildPackages = async function (){
    let html_string = "";

    data_list = await  new_reservation.getReservations();
    

    for(let i =0 ; i < data_list.length ; i++){

        html_string = html_string + `
        <div class="package">
            <div class = "package-wall">
                <img class = "package-img" src="../${data_list[i].image}" alt="">
                <div class="package-price">Since<br>$${data_list[i].price}</div>
                <h3 class="package-days">${data_list[i].days}</h3>
            </div>
            <h1 class="package-name">${data_list[i].name}</h2>
            
            <h3 class= "package-country">${data_list[i].country}</h3>
           
            <h3 class="package-cliente">Cliente: ${data_list[i].cliente_id.nombre}</h3>
            <h3 class="package-cliente">Persons: ${data_list[i].count}</h3>
            <h3 class="package-cliente">Date: ${data_list[i].date}</h3>
            <h3 class="package-cliente">Aditional: ${data_list[i].detalles}</h3>
        </div>
    `
    }

    return html_string
}
const packageContent = await buildPackages() 

packagesBox.innerHTML = packageContent;