import { generateAreaReports } from "./components/rowDataReport.js";

let value;
let texto;

const btnGetReportData = document.getElementById("btn-getdata-report")
const optionsSelect = document.getElementById("options-report")
const dataContent = document.getElementById("data-report")
const btnPrintReport = document.getElementById("btn-print-report"); 

const getClients = function(){
    return fetch("http://localhost:3000/clients")
    .then(response => response.json())
    .then(data => {
        return data
    })
}

const getPackages = function(){
    return fetch("http://localhost:3000/packages")
    .then(response => response.json())
    .then(data => {
        return data
    })
}


const getReservations= function (){
    return fetch("http://localhost:3000/reservations")
    .then(response => response.json())
    .then(data => {
        return data
    })
}





btnGetReportData.addEventListener('click',(e)=>{
    let data_db;
    e.preventDefault();
    value= optionsSelect.options[optionsSelect.selectedIndex].value;
    if(value == "clients"){
        data_db = getClients()
        data_db.then((data) => {
             texto = generateAreaReports(data, value);
            dataContent.innerHTML = texto
        })
    }
    if(value == "packages"){
        data_db = getPackages()
        data_db.then((data) => {
             texto = generateAreaReports(data, value);
            dataContent.innerHTML = texto
        })
    }
    if(value == "reservations"){
        data_db = getReservations()
        data_db.then((data) => {
             texto = generateAreaReports(data, value);
            dataContent.innerHTML = texto
        })
    }
    
})


// document.addEventListener("DOMContentLoaded", () => {
//     let btnPrintReport = document.getElementById("btn-print-report");
//     let container = document.getElementById("data-report");
 
//     btnPrintReport.addEventListener("click", event => {
//         event.preventDefault();
//         btnPrintReport.style.display = "none";
//         window.print();
//     }, false);
 
//     container.addEventListener("click", event => {
//         btnPrintReport.style.display = "initial";
//     }, false);
 
// }, false);




function printDiv(divId, title) {

  let mywindow = window.open('', 'PRINT', 'height=650,width=900,top=100,left=150');

  mywindow.document.write(`<html><head><title>${title}</title>`);
  mywindow.document.write('</head><body >');
  mywindow.document.write(document.getElementById(divId).innerHTML);
  mywindow.document.write('</body></html>');

  mywindow.document.close(); // necessary for IE >= 10
  mywindow.focus(); // necessary for IE >= 10*/

  mywindow.print();


  return true;
}


//data-report

function printpage() {
    var printButton = document.getElementById("btn-print-report");
    printButton.style.visibility = 'hidden';
    document.title = "FinalReport";
    window.print()
}


btnPrintReport.addEventListener('click',(e)=>{
    printpage()
    location.assing("./reports.html")
    // console.log("printing")
})

