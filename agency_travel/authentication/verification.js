import { createAdminSesion, createEmployeeSesion } from "./auth.js";


const email = document.getElementById("email")
const password = document.getElementById("pass")
const btnLogin = document.getElementById("btnlogin")

let ROL = "GUEST"

const employees = fetch("http://localhost:3000/employees")
.then(response => response.json())
.then(data => {
    return data
})

const changeRol = function(id_rol){
  if(id_rol == 1){
    ROL = "ADMIN"
  }
  if(id_rol == 2){
    ROL = "USER"
  }
}

const validateUser = function (data_list){
  for(let i =0 ; i < data_list.length ; i++){
    if(data_list[i].email == email.value && data_list[i].password == password.value){
      changeRol(data_list[i].type_employee)

      if(ROL == "ADMIN"){
        createAdminSesion()
        console.log("logged admin");
        location.assign("../home.html");
        
      }
      if(ROL == "USER"){
        createEmployeeSesion()
        console.log("logged user");
        location.assign("../home.html");
        
      }
      break;
    }
  }
}

const getDataAndValidate = function (){
  employees.then((data)=>{
    validateUser(data)
  })
}

btnLogin.addEventListener('click',(e)=>{
  e.preventDefault();
  getDataAndValidate();

})
