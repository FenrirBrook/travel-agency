export function verifySesion(){
    if(localStorage.getItem("ROL") == "ADMIN" || localStorage.getItem("ROL") == "USER"){
        return true;
    }
    return false;
}

export function createAdminSesion(){
localStorage.setItem("ROL","ADMIN");
}

export function verifySesionAdmin(){
    if(getRolSesion() == 'ADMIN'){
        return true
    }
    return false
}

export function createEmployeeSesion(){
    localStorage.setItem("ROL","USER");
}

export function verifySesionEmployee(){
    if(getRolSesion() == 'USER'){
        return true
    }
    return false
}


export function getRolSesion(){
    return localStorage.getItem("ROL")
}

export function modify(){
    localStorage.setItem("ROL","new_value");
}

export function setGuest(){
    localStorage.setItem("ROL","GUEST");
}



export function delete_(){
    localStorage.removeItem("ROL");
}
