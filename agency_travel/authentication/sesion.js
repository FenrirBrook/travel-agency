import { verifySesionAdmin } from "./auth.js";

if(!verifySesionAdmin()){
    location.assign("../home.html");
}
