export class Conection{

    constructor(str_entity){
        this.str_entity = str_entity
        this.url = `http://localhost:3000/${this.str_entity}`
    }

    getData(){
        return fetch(this.url)
        .then(response => response.json())
        .then(data => {
            return data
        })
    }

    getElementId(idObj){
        return fetch(this.url + "/" + idObj,{method: "GET"})
        .then(response => response.json())
        .then(data => {
            return data
        })
    }

    addElement(objEntity){
        fetch(this.url,{
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(objEntity)
        })
    }

    updateElement(objEntity){
        fetch(this.url + "/" + objEntity.id,{
            headers: {
                'Content-Type': 'application/json'
            },
            method: "PUT",
            body: JSON.stringify(objEntity)
        })
    }

    deleteElement(objEntity){
        fetch(this.url + "/" + objEntity.id,{
            method: "DELETE"
        }).then((a) =>{
            return a
        })

    }

}