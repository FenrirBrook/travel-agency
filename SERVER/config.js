const clients = require('../A_DATABASE/clients.json');
const employees = require('../A_DATABASE/employees.json');
const countries = require('../A_DATABASE/countries.json');
const packages = require('../A_DATABASE/packages.json');
const reservations = require('../A_DATABASE/reservations.json');
const type_employees = require('../A_DATABASE/type_employees.json');

module.exports = () => ({
  ...clients,
  ...employees,
  ...countries,
  ...packages,
  ...reservations,
  ...type_employees
});