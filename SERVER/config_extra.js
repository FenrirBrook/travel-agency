const jsonServer = require("json-server");
const clients = require('./A_DATABASE/clients.json');
const countries = require('./A_DATABASE/countries.json');

const clonedData = {
  ...clients,
  ...countries
}; 
const app = jsonServer.create();
const router = jsonServer.router(clonedData, {_isFake: true});

app.use((req, res, next) => {
  if (req.path === "/") return next();
  console.log("::paths is console loggedd::", req.path);
  router.db.setState(clone(clonedData));
  next();
});

const isDevelopment = process.env.NODE_ENV !== "production";
app.use(
  jsonServer.defaults({
    logger: isDevelopment
  })
);
console.log("::isDevelopment::", isDevelopment);
app.use(router);

module.exports = app;
